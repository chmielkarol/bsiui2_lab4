﻿#include "pch.h"
#include <iostream>
#include <string>

bool PatchIfZadanie3ExeVariant(FILE * pFile)
{
	char byte;

	fseek(pFile, 0xb5091, SEEK_SET);
	size_t bytesRead = fread(&byte, 1, 1, pFile);
	if (bytesRead != 1) return false;

	if (byte == (char)0x75)
	{
		fseek(pFile, 0xb5091, SEEK_SET);
		fputc(0xeb, pFile);
		return true;
	}
	return false;
}

bool PatchIfZadanie3AltExeVariant(FILE * pFile)
{
	char bytes[2];

	fseek(pFile, 0xb6d, SEEK_SET);
	size_t bytesRead = fread(bytes, 1, 2, pFile);
	if (bytesRead != 2) return false;

	if (bytes[0] == (char)0x0f && bytes[1] == (char)0x84)
	{
		fseek(pFile, 0xb6e, SEEK_SET);
		fputc(0x85, pFile);
		return true;
	}
	return false;
}

int main()
{
	//Get file name
	std::cout << "Address to the program to be patched: ";
	char fileName[50];
	std::cin >> fileName;

	//Open file
	FILE * pFile;
	pFile = fopen(fileName, "r+b");
	if (pFile == NULL)
	{
		std::cout << "File cannot be opened";
		return -1;
	}

	bool programPatched = PatchIfZadanie3ExeVariant(pFile);
	if (programPatched)
	{
		//Write success message
		std::cout << "Your program has been patched successfully.";

		//Close file
		fclose(pFile);
		return 0;
	}

	programPatched = PatchIfZadanie3AltExeVariant(pFile);
	if (programPatched)
	{
		//Write success message
		std::cout << "Your program has been patched successfully.";

		//Close file
		fclose(pFile);
		return 0;
	}

	//Unrecognized format message
	std::cout << "Your program has an unrecognized format and cannot been patched.";
	//Close file
	fclose(pFile);

	return 1;
}
